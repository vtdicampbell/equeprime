package com.vtdi.mobile.equeprime;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.vtdi.mobile.equeprime.location.LocationListActivity;

public class MainActivity extends AppCompatActivity {


    private Button buttonHospital;
    private Button buttonPolice;
    private Button buttonFire;
    private Button buttonPanic;
    private Button buttonSettings;
    private Button buttonDavid;
    private Button buttonHorace;
    private Button buttonGrant;
    private Button buttonNicholas;
    private Button buttonJackson;
    private Button buttonOnicka;


    private Button buttonMike;
    private Button buttonWolfe;
    private Button buttonDenarad;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Police
        initPolice();

        //Fire
        initFire();

        //Hospital
        initHospital();

        //Panic
        initPanic();

        //PSettings
        initSettings();

        actionButtonDavid();

        actionButtonHorace();

        actionButtonGrant();

        actionButtonNicholas();

        actionbuttonJackson();

        actionbuttonOnicka();

        actionButtonOthers();
    }

    private void actionButtonOthers() {

        buttonMike = (Button) findViewById(R.id.buttonMike);
        buttonMike.setOnClickListener( new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, MikeActivity.class);

                startActivity(intent);

            }
        });

        buttonWolfe = (Button) findViewById(R.id.buttonWolfe);
        buttonWolfe.setOnClickListener( new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, WolfeActivity.class);

                startActivity(intent);

            }
        });


        buttonDenarad = (Button) findViewById(R.id.buttonDenard);
        buttonDenarad.setOnClickListener( new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, DennardActivity.class);

                startActivity(intent);

            }
        });
    }

    private void actionbuttonOnicka() {
        buttonOnicka = (Button) findViewById(R.id.buttonOnicka);
        buttonOnicka.setOnClickListener( new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, OnickaActivity.class);

                startActivity(intent);

            }
        });


    }

    private void actionbuttonJackson() {
        buttonJackson = (Button) findViewById(R.id.buttonJackson);
        buttonJackson.setOnClickListener( new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, JacksonActivity.class);

                startActivity(intent);

            }
        });



    }

    private void actionButtonNicholas()
    {
        buttonNicholas = (Button) findViewById(R.id.buttonNicholas);
        buttonNicholas.setOnClickListener( new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, NicholasActivity.class);

                startActivity(intent);

            }
        });
    }

    private void actionButtonGrant()
    {
        buttonGrant = (Button) findViewById(R.id.buttonGrant);
        buttonGrant.setOnClickListener( new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, GrantActivity.class);

                startActivity(intent);

            }
        });
    }

    private void actionButtonHorace() {

        buttonHorace = (Button) findViewById(R.id.buttonHorace);
        buttonHorace.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, HoraceActivity.class);

                startActivity(intent);

            }
        });
    }

    private void actionButtonDavid() {

        buttonDavid = (Button) findViewById(R.id.buttonDavid);

        buttonDavid.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, DavidActivity.class);

                startActivity(intent);

            }
        });


    }

    private void initSettings() {

        buttonSettings = (Button) findViewById(R.id.buttonSettings);

        buttonSettings.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();

                Intent intentSettings = new Intent(context, SettingsActivity.class);

                startActivity(intentSettings);


//                Context context = getApplicationContext();
//
//                Intent intent = new Intent(context, HospitalActivity.class);
//
//                startActivity(intent);

            }
        });

    }

    private void initPanic() {

         buttonPanic = (Button) findViewById(R.id.buttonPanic);

        buttonPanic.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();
                CharSequence text = "HELP! HELP!";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();


                //hide buttons
                //showServiceButtons(false);

            }


        });

    }

    private void showServiceButtons(boolean show) {

        if(show){
            buttonHospital.setVisibility(View.VISIBLE);
            buttonFire.setVisibility(View.VISIBLE);
            buttonPolice.setVisibility(View.VISIBLE);
        }else{
            buttonHospital.setVisibility(View.GONE);
            buttonFire.setVisibility(View.GONE);
            buttonPolice.setVisibility(View.GONE);
        }


    }

    private void initHospital() {

        buttonHospital = (Button) findViewById(R.id.buttonHospital);


        buttonHospital.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, LocationListActivity.class);

                intent.putExtra("LOCATION_TYPE", "hospital");

                startActivity(intent);

            }
        });

    }

    private void initPolice() {

        buttonPolice = (Button) findViewById(R.id.buttonPolice);

        buttonPolice.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();

                Intent intent = new Intent(context, LocationListActivity.class);

                intent.putExtra("LOCATION_TYPE", "police");

                startActivity(intent);

            }
        });

    }

    private void initFire() {


        buttonFire = (Button) findViewById(R.id.buttonFire);

        buttonFire.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Context context = getApplicationContext();

                Intent intent = new Intent(context, LocationListActivity.class);

                intent.putExtra("LOCATION_TYPE", "fire_station");

                startActivity(intent);




            }
        });

    }


}
