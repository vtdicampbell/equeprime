package com.vtdi.mobile.equeprime.location;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vtdi.mobile.equeprime.OnickaActivity;
import com.vtdi.mobile.equeprime.lists.PoliceAdapter;

/**
 * Created by jerome on 7/3/2018.
 */

public class LocationListActivity extends ListActivity {

    private static final String TAG = "ListActivity";

    static final String[] HOSPITALS =
            new String[] { "UWI", "KPH", "St.Andrew", "Private"};

    static final String[] FIRE_STATIONS =
            new String[] { "YORK PARK", "HALF WAY TREE", "PAPINE", "Private"};

    static final String[] POLICE =
            new String[] { "PAPINE", "CROSS ROADS", "DOWNTOWN", "Private"};


    static final String[] EMPTY_LIST =
            new String[] { "EMPTY" };


    private String locationType;

    //testing out
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.activity_police);


        locationType = getIntent().getStringExtra("LOCATION_TYPE");


        if(locationType.equals("hospital")){

            setListAdapter(new LocationAdapter(this, HOSPITALS));

        }
        else  if(locationType.equals("fire_station"))
        {
            setListAdapter(new LocationAdapter(this, FIRE_STATIONS));
        }

        else  if(locationType.equals("police"))

        {
            setListAdapter(new LocationAdapter(this, POLICE));
        }else{


            setListAdapter(new LocationAdapter(this, EMPTY_LIST));

        }






        ListView listView = getListView();
        listView.setTextFilterEnabled(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Toast.makeText(getApplicationContext(),
                        "Item Clicked", Toast.LENGTH_SHORT).show();

                Context context = getApplicationContext();

                Intent intent = new Intent(context, LocationDetailsActivity.class);

                startActivity(intent);

            }
        });
    }

    private void initView() {
    }
}
